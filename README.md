HelloWorld
==========
Una aplicacion simple HTML

Descripcion de funcionamiento:

- Cuando se realiza un cambio sobre el codigo html:

Se va a ejecutar un pipeline automatico de CICD llamado gitlab-ci.yml 

- Este archivo consta de los siguientes stages:        
    - **build**: llama un servidor apache2 tomcat desde un Dockerfile y construye una imagen de docker con la pagina index.HTML - luego de crear la imagen se realiza un test con trivy sobre dicha imagen y la sube al registry de DockerHub
    -  **test**: Se realiza un test trivy sobre el repositorio completo en busca de vulnerabilidades principalmente sobre librerias que los dev vayan creando
    - **deploy**: se realiza un kubectl apply de los archivos de deployment con nuestra imagen de docker para poder levantarla en el cluster de kubernetes.



---------------------
---------------------
---------------------

- Nuestro primer objetivo fue "Lograr un ciclo completo de CICD utilizando GitLab"													
- Nos subdividimos en subgrupos para investigar, desarrollar e implementar alguna solucion parcial.													
- asi fue que Logramos.													
	- por un lado desplegar una pequeña aplicacion en Java en un entorno local												
	- y por otro lado desplegar un servidor apache que mostraba solo un index.html												
- Luego de eso nos planteamos como objetivo automatizar eso mismo que habiamos desarrollado en un entorno local													
- despues de mucho esfuerzo logramos hacer funcionar un runner de gitlab y automatizamos el proceso para desplegarse en una VM.													
- El siguiente desafío fue "hacer eso mismo" pero en un contenedor.													
- comenzamos a investigar un poco mas en profundidad Docker													
- creamos un Dockerfile													
- pudimos hacer un build y crear nuestra propia imagen con un NODE para el backend y un APACHE para el frontend													
- modificamos el pipeline de CICD para automatizar el proceso de despliegue del backend y el frontend al registrar algun cambio.													
- paralelamente fuimos avanzando en investigar como implementar un cluster de kubernetes que permitiera trasladar todos esos avances a un ambiente que nos permitiera ingregar estas aplicaciones con otras herramientas.													
- despues de varios dias y mucho esfuerzo logramos creamos el cluster en Google cloud y conectarlo con el runner de Gitlab utilizando HELM													
- logramos subir las imagenes generadas anteriormente a dockerhub													
- logramos implementar los deployments en la nube de Google, generando los pods con el puerto corresponiente para poder visualizar la pagina html													
- elegimos Lens para visualizar el cluster de kubernetes.													
- se instala con Helm prometheus y grafana pero por cuestiones de tiempo no llegamos a conectar con nuestra pagina HTML													
